all: singlethread_translator doublethread_translator triplethread_translator

triplethread_translator: translator_triple.o
	gcc translator_triple.o -o triplethread_translator -lz -lpthread

doublethread_translator: translator_double.o
	gcc translator_double.o -o doublethread_translator -lz -lpthread

singlethread_translator: translator_single.o
	gcc translator_single.o -o singlethread_translator -lz

translator_triple.o: translator_triple.c
	gcc -g -Wall -c translator_triple.c

translator_double.o: translator_double.c
	gcc -g -Wall -c translator_double.c

translator_single.o: translator_single.c
	gcc -g -Wall -c translator_single.c

clean:
	rm -rf *\.o singlethread_translator doublethread_translator triplethread_translator
