#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

int main(int argc, char *argv[])
{
	clock_t start_t, end_t;

	start_t = clock();

	//if not enough parameters on command line
 	if(argc < 2)
	{
 	   	printf("Usage:\n\t%s filename\n",argv[0]);
  	  	return -1;
 	}

	//Open input file and check for error
	gzFile input_file = gzopen(argv[1], "r");
    	if (input_file == NULL) 
	{
        	printf ("Trying to open '%s' failed.\n", argv[1]);
            	exit (EXIT_FAILURE);
    	}


	//open output file and check for error
	gzFile output_file = gzopen(argv[2], "w6");

    	if (output_file == NULL) 
	{
        	printf ("Trying to open '%s' failed.\n", argv[2]);
  		gzclose(input_file); // close input file
            	exit (EXIT_FAILURE);
    	}

    	
 	while (1) 
	{
        	int err;                    
        	int bytes_read;
		const int BUFFER_SIZE = 8192;
		int i;

		//buffer
		char buf0 [BUFFER_SIZE];

		//get some input
		bytes_read = gzread (input_file, buf0, BUFFER_SIZE);

		//buf0[bytes_read] = '\0';

		//translate into modern language
		for (i = 0;i < BUFFER_SIZE;i++)
		{
			if (buf0[i] == 'a' || buf0[i] == 'A')
			{
				buf0[i] = '4';
			}
			else if (buf0[i] == 'e' || buf0[i] == 'E')
			{
				buf0[i] = '3';
			}
			else if (buf0[i] == 'i' || buf0[i] == 'I')
			{
				buf0[i] = '1';
			}
			else if (buf0[i] == 'o' || buf0[i] == 'O')
			{
				buf0[i] = '0';
			}
			else if (buf0[i] == 's' || buf0[i] == 'S')
			{
				buf0[i] = '5';
			}
		}

		//do some output
		gzwrite(output_file, buf0, bytes_read);


		//check for end of file or error
		if (bytes_read < BUFFER_SIZE - 1) 
		{
		    	if (gzeof (input_file)) 
			{
		        	break;
		    	}
		    	else 
			{
				const char* error_string;
				error_string = gzerror (input_file, & err);
				if (err) 
				{
				    	fprintf (stderr, "Error: %s.\n", error_string);
					//free up resources
  					gzclose(input_file); // close input file
					gzclose(output_file); // close output file
				    	exit (EXIT_FAILURE);
		        	}
		    	}
		}
    	}

	//free up resources
  	gzclose(input_file); // close file
	gzclose(output_file); // close file


	end_t = clock();

	printf("Start time = %ju\n", (uintmax_t)start_t);
	printf("End time = %ju\n", (uintmax_t)end_t);

	printf("Number of clock cycles = %ju\n", (uintmax_t)(end_t - start_t));

	return 0;


}


