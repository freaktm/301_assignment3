#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

//CONSTANTS
const int NUMBER_OF_BUFFERS = 20;
const int BUFFER_SIZE = 8192;

//GLOBAL VARIABLES
char buf[20][8192];
int bytes_read[20];
int buf_status[20];

//CONDITION VARIABLE
pthread_cond_t buf_condition[20];

//MUTEX
pthread_mutex_t buf_mutex[20];

//gzFile's
gzFile input_file;
gzFile output_file;

//EOF variable
int eof = -1;

// function prototypes
void* read_data ();
void* write_data ();



int main(int argc, char *argv[])
{
	//BENCHMARK VARIABLES
	clock_t start_t, end_t;
	start_t = clock();

	//ERROR CHECKING OF INPUT
	//if not enough parameters on command line
 	if(argc < 2)
	{
 	   	printf("Usage:\n\t%s filename\n",argv[0]);
  	  	return -1;
 	}
	//Open input file and check for error
	input_file = gzopen(argv[1], "r");
    	if (input_file == NULL) 
	{
        	printf ("Trying to open '%s' failed.\n", argv[1]);
            	exit (EXIT_FAILURE);
    	}
	//open output file and check for error
	output_file = gzopen(argv[2], "w6");

    	if (output_file == NULL) 
	{
        	printf ("Trying to open '%s' failed.\n", argv[2]);
  		gzclose(input_file); // close input file
            	exit (EXIT_FAILURE);
    	}

	//INIT VARIABLES
	int i;
  	long t1=1, t2=2;

	//create array for threads
  	pthread_t threads[2];
  	pthread_attr_t attr;

	//INIT MUTEX'S AND CONDITION VARIABLES'S
	for (i = 0; i < NUMBER_OF_BUFFERS;i++)
	{
		pthread_mutex_init(&buf_mutex[i], NULL);
		pthread_cond_init (&buf_condition[i], NULL);
		bytes_read[i] = 0;
		buf_status[i] = 0;
	}


  	//START THREADS
  	pthread_attr_init(&attr);
  	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  	pthread_create(&threads[0], &attr, read_data, (void *)t1);
  	pthread_create(&threads[1], &attr, write_data, (void *)t2);

  	//WAIT FOR THREADS TO FINISH
  	for (i=0; i<2; i++) 
	{
    		pthread_join(threads[i], NULL);
  	}
	
  	//CLEAN UP THREADS
  	pthread_attr_destroy(&attr);
	for (i = 0; i < NUMBER_OF_BUFFERS;i++)
	{
	  	pthread_mutex_destroy(&buf_mutex[i]);
	  	pthread_cond_destroy(&buf_condition[i]);
	}

	//FREE UP FILE RESOURCES
  	gzclose(input_file); // close file
	gzclose(output_file); // close file
	
	//DO BENCHMARK STUFF
	end_t = clock();

	printf("Start time = %ju\n", (uintmax_t)start_t);
	printf("End time = %ju\n", (uintmax_t)end_t);

	printf("Number of clock cycles = %ju\n", (uintmax_t)(end_t - start_t));

	//EXIT PROGRAM
	pthread_exit(NULL);

}


//function for read_data thread
void* read_data()
{
	//declare a counter for bufferId
	int bufferId = 0;
	int finished = 0;

	//process file until finished
	while(finished == 0)
	{     
		// error data
		int err;
		//set byte counter
        	int total_bytes_read = 0;       

		//get mutex for buffer based on bufferId
		pthread_mutex_lock(&buf_mutex[bufferId]);

		//check to make sure buffer is not full
		if(buf_status[bufferId] > 0)
		{
			pthread_cond_wait(&buf_condition[bufferId], &buf_mutex[bufferId]);
		}

		//get some input & store in buffer		
		total_bytes_read = gzread (input_file, buf[bufferId], BUFFER_SIZE);	

		//update bytes_read array
		bytes_read[bufferId] = total_bytes_read;

		//update status
		buf_status[bufferId] = 1;

		//release mutex
      		pthread_cond_signal(&buf_condition[bufferId]);
    		pthread_mutex_unlock(&buf_mutex[bufferId]);



		//check for end of file
		if (total_bytes_read < BUFFER_SIZE - 1) 
		{
		    	if (gzeof (input_file)) 
			{
		        	finished = 1;
		    	}
		    	else 
			{
				const char* error_string;
				error_string = gzerror (input_file, & err);
				if (err) 
				{
				    	fprintf (stderr, "Error: %s.\n", error_string);
					//free up resources
  					gzclose(input_file); // close input file
					gzclose(output_file); // close output file
				    	pthread_exit(&err);
		        	}
		    	}
			eof = bufferId;
		} 
		else
		{
			//increment bufferId
			if(bufferId < NUMBER_OF_BUFFERS-1)
			{
				bufferId++;
			}
			else
			{
				bufferId = 0;
			}
		}
		
	}

	//exit thread
	pthread_exit(NULL);
	
}





//function for write_data thread
void* write_data()
{
	//declare a counter for bufferId
	int bufferId = 0;
	int finished = 0;

	//process file until finished
	while(finished == 0)
	{     
		int i;    

		//get mutex for buffer based on bufferId
		pthread_mutex_lock(&buf_mutex[bufferId]);

		//check to make sure buffer is not full
		if(buf_status[bufferId] == 0)
		{
			pthread_cond_wait(&buf_condition[bufferId], &buf_mutex[bufferId]);
		}

		//translate buffer
		for (i = 0; i < bytes_read[bufferId];i++)
		{
			if (buf[bufferId][i] == 'a' || buf[bufferId][i] == 'A')
			{
				buf[bufferId][i] = '4';
			}
			else if (buf[bufferId][i] == 'e' || buf[bufferId][i] == 'E')
			{
				buf[bufferId][i] = '3';
			}
			else if (buf[bufferId][i] == 'i' || buf[bufferId][i] == 'I')
			{
				buf[bufferId][i] = '1';
			}
			else if (buf[bufferId][i] == 'o' || buf[bufferId][i] == 'O')
			{
				buf[bufferId][i] = '0';
			}
			else if (buf[bufferId][i] == 's' || buf[bufferId][i] == 'S')
			{
				buf[bufferId][i] = '5';
			}
		}

	
		//do some output
		gzwrite(output_file, buf[bufferId], bytes_read[bufferId]);	


		//update status
		buf_status[bufferId] = 0;

		//release mutex
      		pthread_cond_signal(&buf_condition[bufferId]);
    		pthread_mutex_unlock(&buf_mutex[bufferId]);



		//check for end of file
		if (eof != bufferId) 
		{
			//increment bufferId
			if(bufferId < NUMBER_OF_BUFFERS-1)
			{
				bufferId++;
			}
			else
			{
				bufferId = 0;
			}
		} 
		else
		{
			finished = 1;
		}
	}

	//exit thread
	pthread_exit(NULL);	
}


